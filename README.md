# DCCJS-level-crossing
A program to control several level-crossing.

Each level crossing is managed by two pins:
* A servo for closing and opening the gates when a train arrives
* A blinking led.

The program is controlled through the i2c bus. The following operations are available:
* controls of the constants used for each level-crossing:
  * the min and max value used for the servo .
  * the blinking period
  * the grace period (blinks befor the gate closes and after it is opened).
  * the speed of the gates
* close and open gate signal for each level-crossing

The code will execute on an Arduino nano or uno. It can control 6 level-crossing.
The manager must send one byte i2c commands eventually followed by arguments.
The five high-end bits in the command byte identify the actions. The 3 low-end bits 
identify the level-crossing (correct values from 0 to 5).

Linking a gate with the next one means that any action (opening or closing) on the
gate will also be performed on the next one. It can be used to attach two servos to
a single level crossing.

| Prefix  | arguments | Operation                                                              |
|---------|-----------|------------------------------------------------------------------------|
| 0x10    | 0xXX 0xYY | Attach with servo opened at XX * 8+500 us and closed at YY * 8 + 500 us|
| 0x18    |           | Disable level crossing                                                 |
| 0x20    |           | Close the gate                                                         |
| 0x28    |           | Opens the gate                                                         |
| 0x30    |           | Link current gate with next one                                        |
| 0x38    |           | Unlink current gate with next one                                      |

The I2C address of the Arduino is 0x12 (constant in main.c).
It uses digital pins 2 to 7 for servos and 8 to 13 for leds. A4 and A5 are used for
i2c.
