OBJ=i2c-slave.o level_crossing.o main.o
DEV=ttyUSB0
CC=avr-gcc
CFLAGS=-Os -DF_CPU=16000000UL -mmcu=atmega328p
TARGET=dccjs-level-crossing
all: $(TARGET) install

$(TARGET): $(OBJ)
	avr-gcc -mmcu=atmega328p $^ -o $(TARGET)
	avr-objcopy -O ihex -R .eeprom $(TARGET) $(TARGET).hex
install:
	avrdude -F -V -c arduino -p ATMEGA328P -P /dev/$(DEV) -b 57600 -U flash:w:$(TARGET).hex
clean:
	rm $(OBJ) $(TARGET) $(TARGET).hex
