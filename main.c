#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "i2c-slave.h"
#include "level_crossing.h"

/* Constant: I2C slave address of the arduino */
#define SLAVE_ADDRESS 0x12

void onWrite(int len) {
}

/** A bitfield that identifies linked crossings */
char linked = 0;

void onRead(int len, uint8_t buf []) {
    if(len <= 0) return;
    uint8_t cid = buf[0] & 0x7;
    if (cid >= 6) return;
    uint8_t opened;
    uint8_t closed;
    switch(buf[0] & 0xF8) {
        case 0x10:
            opened = buf[1];
            closed = buf[2];
            attach_crossing(cid, (uint16_t) (opened * 8 + 500), (uint16_t) (closed * 8 + 500));
            break;
        case 0x18:
            detach_crossing(cid);
            if(cid < 5 && (linked & (1 << cid))) {
                detach_crossing(cid + 1);
            }
            break;
        case 0x20:
            close_crossing(cid);
            if(cid < 5 && (linked & (1 << cid))) {
                close_crossing(cid + 1);
            }
            break;
        case 0x28:
            open_crossing(cid);
            if(cid < 5 && (linked & (1 << cid))) {
                open_crossing(cid + 1);
            }
            break;
        case 0x30:
            linked |= 1 << cid;
            break;
        case 0x38:
            linked &= ~(1 << cid);
            break;
        case 0x40:
            opened = buf[1];
            set_crossing(cid, (uint16_t) (opened * 8 + 500));
            break;
        default:
            ;;
    }
}

int current;

void init() {
    cli();
    init_crossing();
    i2c_set_out((uint8_t *)&current, 2);
    i2c_init(SLAVE_ADDRESS, onRead, onWrite);
    sei();
}

void loop() {
    _delay_ms(500.0);
}

void main() {
    init();
    while(1) loop();
}
