/* (c) 2018 crepi22 
 * Redistributed under the terms of the BSD 3 clause license 
 */

#ifndef _I2C_SLAVE_H
#define _I2C_SLAVE_H

#include <stdint.h>

typedef void (*ReceiveCallback)(int length, uint8_t buffer[]);
typedef void (*WriteCallback)(int length);

void i2c_set_out(uint8_t *buffer, uint8_t size);
void i2c_init(uint8_t address, ReceiveCallback rcb, WriteCallback wcb);

#endif /* i2c_slave.h */