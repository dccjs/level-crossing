/* (c) 2018 crepi22
 * Redistributed under the terms of the BSD 3 clause license
 */

#ifndef _SERVO_H
#define _SERVO_H
#include <stdint.h>

/** Initialize the whole library */
void init_crossing();
/** configure and register a new crossing */
void attach_crossing(uint8_t cid, uint16_t opened, uint16_t closed);
/** unregister a crossing */
void detach_crossing(uint8_t cid);
/** launch a close */
void close_crossing(uint8_t cid);
/** launch an open */
void open_crossing(uint8_t cid);
/** direct value of servo for testing */
void set_crossing(uint8_t cid, uint16_t val);

#endif