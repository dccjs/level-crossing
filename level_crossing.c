#include "level_crossing.h"
#include <avr/interrupt.h>

typedef enum {
  OPENED,
  CLOSING_BLINKING,
  CLOSING_GATE,
  CLOSED,
  OPENING_GATE,
} state;

typedef struct {
  uint8_t enabled;
  uint16_t pulse_width;
  state state;
  uint16_t closed;
  uint16_t opened;
  uint16_t gate_clock;
  uint16_t blink_clock;
} crossing;

/* number of timer ticks per microsecond with prescale of 8
  (usually 2 except if on 3.3v or internal clock: 1) */
#define TICKS_PER_uS (F_CPU / 1000000 / 8)
// main looping interval
#define SERVO_REFRESH_RATE 20000 * TICKS_PER_uS
#define NOOP_DELAY 16

#define SIZE 6
#define BLINKING_PERIOD (3 * SIZE)
#define GRACE_PERIOD (38 * SIZE)
#define CLOSING_PERIOD (32 * SIZE)

static crossing crossings[SIZE];
/* servo refreshed or SIZE at end of period */
static volatile int8_t cid1;
/* crossing state refreshed (servo pulse width + led blinking) */
static volatile int8_t cid2;

SIGNAL(TIMER1_COMPA_vect) {
  if (cid1 == SIZE) {
    TCNT1 = 0;
    for(cid2=0; cid2 < SIZE; cid2++) {
      crossing *crs = &crossings[cid2];
      if (crs->enabled == 1) {
        if (crs->state != OPENED) {
          if (crs->blink_clock++ == BLINKING_PERIOD) {
            crs->blink_clock = 0;
            PORTB ^= _BV(cid2);
          }
        }
        switch (crs->state) {
          case CLOSING_BLINKING:
            if (crs->gate_clock++ == GRACE_PERIOD) {
              crs->state = CLOSING_GATE;
              crs->gate_clock = 0;
            }
            break;
          case CLOSING_GATE:
            if (crs->gate_clock++ >= CLOSING_PERIOD) {
              crs->state = CLOSED;
              crs->gate_clock = 0;
              crs->pulse_width = crs->closed;
            } else {
              crs->pulse_width =
                  crs->opened + ((int32_t)crs->closed - crs->opened) *
                                            crs->gate_clock / CLOSING_PERIOD;
            }
            break;
          case OPENING_GATE:
            if (crs->gate_clock++ >= CLOSING_PERIOD) {
              crs->state = OPENED;
              crs->gate_clock = 0;
              crs->pulse_width = crs->opened;
              PORTB &= ~_BV(cid2);
            } else {
              crs->pulse_width =
                  crs->closed + ((int32_t)crs->opened - crs->closed) *
                                            crs->gate_clock / CLOSING_PERIOD;
            }
            break;
        }
      }
    }
    cid1 = 0;
  } else {
    if (crossings[cid1].enabled == 1) {
      PORTD &= ~_BV(cid1 + 2);
    }
    cid1++;
  }
  if (cid1 < SIZE) {
    if (crossings[cid1].enabled == 1) {
      OCR1A = TCNT1 + crossings[cid1].pulse_width;
      PORTD |= _BV(cid1 + 2);
    } else {
      OCR1A = TCNT1 + NOOP_DELAY;
    }
  } else {
    OCR1A = (unsigned int)SERVO_REFRESH_RATE;
  }
}

void init_crossing() {
  for (int i = 0; i < SIZE; i++) {
    crossings[i].enabled = 0;
  }
  cid1 = 0;
  uint8_t oldSREG = SREG;
  cli();
  // Port 2 to 7 for servos
  DDRD |= 0xFC;
  // Port 8 to 13 for blinking LEDs
  DDRB |= 0x8F;
  // Normal mode - no bit toggle prescale = 8
  TCCR1A = 0;
  TCCR1B = _BV(CS11);
  // enable the output compare interrupt
  TIMSK1 |= _BV(OCIE1A);
  SREG = oldSREG;
}

void attach_crossing(uint8_t cid1, uint16_t opened, uint16_t closed) {
  uint8_t oldSREG = SREG;
  cli();
  crossing *crs = &crossings[cid1];
  crs->opened = opened * TICKS_PER_uS;
  crs->closed = closed * TICKS_PER_uS;
  crs->pulse_width = crs->opened;
  crs->state = OPENED;
  crs->blink_clock = 0;
  crs->gate_clock = 0;
  crs->enabled = 1;
  SREG = oldSREG;
}

void close_crossing(uint8_t cid1) {
  uint8_t oldSREG = SREG;
  cli();
  crossing *crs = &crossings[cid1];
  switch (crs->state) {
    case OPENED:
      crs->state = CLOSING_BLINKING;
      crs->blink_clock = 0;
      crs->gate_clock = 0;
      break;
    case OPENING_GATE:
      crs->state = CLOSING_GATE;
      crs->gate_clock = CLOSING_PERIOD - crs->gate_clock;
      break;
  }
  SREG = oldSREG;
}

void set_crossing(uint8_t cid1, uint16_t val) {
  uint8_t oldSREG = SREG;
  cli();
  crossing *crs = &crossings[cid1];
  crs->state = OPENED;
  crs->pulse_width = val * TICKS_PER_uS;
  SREG = oldSREG;
}

void open_crossing(uint8_t cid1) {
  uint8_t oldSREG = SREG;
  cli();
  crossing *crs = &crossings[cid1];
  switch (crs->state) {
    case CLOSED:
      crs->state = OPENING_GATE;
      crs->gate_clock = 0;
      break;
    case CLOSING_BLINKING:
      crs->state = OPENED;
      crs->gate_clock = 0;
      PORTB &= ~_BV(cid1);
      break;
    case CLOSING_GATE:
      crs->state = OPENING_GATE;
      crs->gate_clock = CLOSING_PERIOD - crs->gate_clock;
      break;
  }
  SREG = oldSREG;
}

void detach_crossing(uint8_t cid1) {
  uint8_t oldSREG = SREG;
  cli();
  PORTB &= ~_BV(cid1);
  crossings[cid1].enabled = 0;
  SREG = oldSREG;
}
